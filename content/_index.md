---
title: "Home"
date: 2021-09-18T16:59:34-03:00
draft: false
---

{{< rawhtml >}}
<h2>Conteúdo</h2>
<p>A disciplina de Sistemas de Informação da Poli-USP aborda diversos conteúdos e ferramentas relacionados à engenharia de Software. Nesse Site, você encontrará materiais relacionados ao curso e tutoriais fornecidos pelos docentes ao longo da disciplina. Visite a página "Sobre" para mais informações e "Tutoriais" para encontrar os guias das principais ferramentas de software utilizadas no curso.</p>
<h3 class="customh3">Git</h3>
<div class="container">
    <div class="description">
        <p>O Git é um sistema de controle de versão usado para rastrear alterações em projetos de software. Ele permite que os desenvolvedores trabalhem em colaboração, mantendo um histórico detalhado das modificações. Com o Git, é possível criar diferentes versões do código (branches) e mesclar as alterações de forma organizada. Isso facilita o desenvolvimento em equipe e a gestão eficiente de projetos de software. Além disso, o Git é frequentemente utilizado em conjunto com plataformas de hospedagem de código, como o GitHub, para facilitar a colaboração e o compartilhamento de código entre equipes de desenvolvimento.</p>
    </div>
    <div class="image">
        <img src="/sisinfo/images/git-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">HTML</h3>
<div class="container">
    <div class="description">
        <p>O HTML, ou Hypertext Markup Language, é uma linguagem utilizada para criar e estruturar páginas web. Composta por elementos chamados de "tags", o HTML define a forma como o conteúdo é apresentado em um navegador. Ele permite a criação de links, a inclusão de imagens, formulários e a organização hierárquica do conteúdo. Além disso, o HTML5, sua versão mais recente, enfatiza a semântica, proporcionando tags específicas para diferentes tipos de conteúdo. Combinado com CSS para estilos e JavaScript para interatividade, o HTML forma a base para o desenvolvimento web.</p>
    </div>
    <div class="image">
        <img src="/sisinfo/images/html-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">CSS</h3>
<div class="container">
    <div class="description">
        <p>CSS, ou Cascading Style Sheets, é uma linguagem de estilo usada em conjunto com o HTML para controlar a apresentação e o design de páginas web. Ele define como os elementos HTML devem ser exibidos no navegador, incluindo aspectos como cores, fontes, layout e espaçamento. Com o CSS, os desenvolvedores podem criar layouts responsivos e atraentes, melhorando a experiência do usuário. Esta linguagem separa o conteúdo da apresentação, facilitando a manutenção e a atualização de um site. Combinado com HTML e frequentemente com JavaScript, o CSS desempenha um papel crucial na criação de páginas web visualmente atraentes e funcionais.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/css-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">SASS</h3>
<div class="container">
    <div class="description">
        <p>Sass (Syntactically Awesome Stylesheets) é uma extensão do CSS que oferece funcionalidades extras para escrever estilos de forma mais eficiente. Ele introduz conceitos como variáveis, aninhamento de seletores e mixins, o que facilita a organização e manutenção de estilos em projetos web. Sass é pré-processado e convertido em CSS padrão antes de ser usado em um site ou aplicativo, ajudando os desenvolvedores a escrever código mais limpo e escalável.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/sass-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">Bootstrap</h3>
<div class="container">
    <div class="description">
        <p>Sass (Syntactically Awesome Stylesheets) é uma extensão do CSS que oferece funcionalidades extras para escrever estilos de forma mais eficiente. Ele introduz conceitos como variáveis, aninhamento de seletores e mixins, o que facilita a organização e manutenção de estilos em projetos web. Sass é pré-processado e convertido em CSS padrão antes de ser usado em um site ou aplicativo, ajudando os desenvolvedores a escrever código mais limpo e escalável.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/bootstrap-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">Django</h3>
<div class="container">
    <div class="description">
        <p>Django é um framework de desenvolvimento web de código aberto, escrito em Python. Ele fornece um conjunto abrangente de ferramentas e funcionalidades que facilitam a criação de aplicações web complexas e robustas. O Django segue o padrão de arquitetura Model-View-Controller (MVC) e enfatiza o princípio do "batteries-included", ou seja, vem com um conjunto de componentes pré-construídos para acelerar o desenvolvimento. Isso inclui funcionalidades como administração automática de banco de dados, autenticação de usuários e um sistema de rotas simples. Graças à sua documentação detalhada e uma comunidade ativa, o Django é uma escolha popular entre os desenvolvedores para a criação de aplicações web escaláveis e seguras.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/django-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">SQLite</h3>
<div class="container">
    <div class="description">
        <p>SQLite é um sistema de gerenciamento de banco de dados relacional de código aberto, conhecido por sua leveza e simplicidade. Ao contrário de muitos outros Sistemas de Gerenciamento de Banco de Dados (SGBD), o SQLite opera diretamente nos arquivos do sistema, eliminando a necessidade de um servidor separado. Ele é frequentemente utilizado em aplicativos que necessitam de um banco de dados local eficiente e não exigem a complexidade de sistemas mais robustos. Sua popularidade se deve à facilidade de integração e à capacidade de oferecer uma solução de armazenamento de dados simples e eficaz.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/sqlite-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">React</h3>
<div class="container">
    <div class="description">
        <p>React é uma biblioteca de código aberto para construção de interfaces de usuário em aplicações web. Desenvolvida e mantida pelo Facebook, ela permite aos desenvolvedores criar componentes reutilizáveis e interativos que formam a interface de um aplicativo. Com uma abordagem baseada em componentes, o React facilita a construção de interfaces complexas, mantendo a organização e a reutilização de código. Além disso, seu modelo de atualização eficiente, conhecido como Virtual DOM, contribui para um desempenho otimizado em aplicações de página única (SPAs) e em tempo real. O React tornou-se uma escolha popular para o desenvolvimento de interfaces de usuário dinâmicas e responsivas em aplicações web modernas.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/react-logo.png" alt="logo"> 
    </div>
</div>
<hr>
<h3 class="customh3">JavaScript</h3>
<div class="container">
    <div class="description">
        <p>JavaScript é uma linguagem de programação de alto nível, versátil e amplamente utilizada para o desenvolvimento de aplicações web interativas. Ela é suportada por todos os navegadores modernos e permite aos desenvolvedores adicionar funcionalidades dinâmicas e interatividade às páginas da web. JavaScript é uma linguagem baseada em eventos, o que significa que pode responder a ações do usuário, como cliques e formulários, de forma dinâmica. Além disso, ela é capaz de manipular o DOM (Document Object Model) para alterar a estrutura e o conteúdo das páginas em tempo real. Com o uso de bibliotecas e frameworks populares, como React, Angular e Vue.js, o JavaScript tornou-se uma parte fundamental do desenvolvimento web moderno, permitindo a criação de experiências de usuário ricas e envolventes.</p>
    </div>
    <div class="about-img">
        <img src="/sisinfo/images/javascript-logo.jpeg" alt="logo"> 
    </div>
</div>
{{< /rawhtml >}}
---
title: "Tutorial de HTML e CSS"
date: 2023-09-28T21:27:57-03:00
draft: false
---

## Aprenda HTML e CSS

### 1. Instalação (precisa?)

O desenvolvimento de front-end corresponde basicamente à criação da interface gráfica de aplicações web. Como você já deve saber, a exibição de uma página da internet é feita através de um browser, como o Chrome, Firefox ou Edge. Todos os browsers têm a capacidade de processar as linguagens HTML, CSS e Javascript, que formam a trinca base para desenvolvimento de front-end.

Assim, se você conseguiu baixar esta apostila, você provavelmente teve que utilizar um desses browsers e, portanto, não necessita de nenhum outro software para visualizar o front-end da aplicação que será desenvolvida. Vamos fazer um experimento: em um diretório de sua escolha, digite em um shell qualquer:

```bash
$ mkdir olamundohtml
$ cd olamundohtml
$ echo "Ola mundo" > index.html
```

Agora, abra o arquivo olamundo/index.html no seu browser padrão. O resultado deve ser igual ao da Fig. abaixo, o que indica que você possui todo ferramentário para o desenvolvimento de front-end.

ℹ️ Por diversos motivos relacionados à evolução das linguagens de front-end, os browsers têm capacidades diferentes em relação ao suporte de funcionalidades de cada linguagem. As funcionalidades introduzidas nesta aula são básicas e, por isso, suportadas por todos os browsers. No entanto, em uma aplicação real, é muito importante levar em conta estas limitações, uma vez que não é possível controlar o browser que o usuário utiliza.

#### 1.1 Visual Studio Code

Como visto, as páginas HTML são arquivos de texto e, portanto, podem ser criadas em qualquer editor de texto. No entanto, como você já deve ter experiência, é mais conveniente utilizar um ambiente integrado de desenvolvimento (IDE), que geralmente possui funções como destaque de sintaxe, autocomplemento, entre outros. Os mais populares para desenvolvimento HTML/CSS/Javascript são:

- Sublime Text
- Atom.io
- Visual Studio Code
- Notepad++ (somente Windows)

Neste curso, adotaremos o Visual Studio Code (a.k.a. VSCode) como IDE nos nossos exemplos, mas você pode escolher outro se preferir. Para instalar o VSCode, é só seguir as instruções na página https://code.visualstudio.com/ (tem para Windows, Linux e MAC). Não é necessária nenhuma extensão adicional para trabalhar com HTML e/ou CSS.

#### Instalando no Windows com o Chocolatey

Se você acompanhou as apostilas anteriores e seguiu os métodos recomendados para instalação, deve ter utilizado o Chocolatey.

Como pode ser verificado em https://chocolatey.org/packages, já existe o pacote do VSCode e, portanto, para instalá-lo, basta digitar o seguinte em um terminal executado como administrador:

```bash
$ choco install vscode
```

#### 1.2 Criando o seu primeiro arquivo HTML

Vamos criar nosso primeiro arquivo HTML de verdade no VSCode. Para tal, crie uma pasta olamundohtml2. Então, abra o VSCode, vá em File->Open Folder e escolha essa pasta. No Windows, uma forma mais conveniente é, no Explorador de Arquivo, abrir o menu de contexto com o botão direito e clicar "Abrir com Code".

Agora, crie um novo arquivo ao clicar no botão indicado na Fig. abaixo e dê o nome de index.html.

Comece a digitar html:5 e o autocompletador deve sugerir o snippet correto. Ao clicar nele, o código deve se expandir para:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```

Iremos explorar esta estrutura mais adiante; por ora, substitua Document por Nossa primeira página HTML válida e insira Olá mundo entre as tags de abertura e fechamento do body:

```html
...
    <title>Nossa primeira página HTML válida</title>
</head>
<body>
    Olá Mundo
</body>
</html>
```

E, assim, temos nossa primeira página HTML válida!

#### 1.3 Visualizando a página

Para visualizar a página, basta abri-la no seu browser, como feito anteriormente, o que resulta em algo semelhante ao exemplo anterior. Se você prestar atenção, entretanto, verá uma pequena diferença: temos um título personalizado para a página.

Outra opção para visualizar a nossa página HTML é utilizar o Live Server, que é uma extensão do VSCode. Para instalá-la, vá em Extensions na aba lateral (ou clique Ctrl + Shift + x), faça uma busca por "Live Server", e clique em install (talvez seja sugerido reiniciar o VSCode).

Ao voltar na aba lateral de navegação dos arquivos Explorer (ou Ctrl + Shift + e), clique com o botão direito no arquivo index.html e selecione "Open with Live Server". Uma janela do browser automaticamente se abrirá com sua página. Agora toda vez que você salvar alguma modificação, a página se atualizará automaticamente. Uma vantagem de executar um servidor web localmente, é poder abrir a página em outro computador/celular na mesma rede, ao especificar a porta 5500. Isso será útil mais a frente quando testarmos design responsivo.

ℹ️ Mas por que estamos nomeando nossos arquivos como index.html? Basicamente, é comportamento padrão de servidores HTTP buscar este arquivo sempre que acessamos uma pasta. Se você está utilizando o Live Server, pode fazer um teste. Crie uma pasta com o nome teste e coloque um arquivo index.html com algum conteúdo nela. Agora abra http://127.0.0.1:5500/teste/ e verifique que o arquivo correto foi aberto. Mude o nome e tente novamente: o resultado será diferente.

### 2. HTML (HyperText Markup Language)

A linguagem HTML serve apenas para descrever a estrutura do texto e seu conteúdo. Ela não é uma linguagem de programação propriamente dita -- não possui variáveis nem controle de lógica. De um modo simplificado, HTML se assemelha mais a uma linguagem para descrever documentos de texto estruturado, como Word ou Latex, mas sem especificações de formatação (fonte, cor) ou layout/posicionamento.

Por exemplo, nesta apostila, o texto é estruturado em capítulos: o primeiro possui o título de instalação, o segundo de HTML, assim por diante. Cada capítulo possui seções e subseções: 1.1, 1.1.1, 1.2, 1.3, 2,1, etc. Uma versão em HTML desta apostila deve refletir esta estrutura, além de especificar alguns outros elementos e sua hierarquia.

#### 2.1 A estrutura de uma página HTML

Antes de incrementar o nosso exemplo "Olá mundo", vamos tentar compreender o básico da estrutura do arquivo HTML que criamos. Relembrando, o arquivo criado tinha mais ou menos o seguinte conteúdo:

```html
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Nossa primeira página HTML válida</title>
    </head>
    <body>
        Olá Mundo
    </body>
</html>
```

Apenas analisando este código, você já deve ter inferido bastante coisa sobre a linguagem HTML. Primeiro, a tabulação (que não é obrigatória) dá a indicação da hierarquia do documento, que pode ser esquematizada como:

```markdown
html
|   head
|   |   title
|   |   endtitle
|   endhead
|   body
|   endbody
endhtml
```

A partir dessa observação, também notamos que cada "seção" começa com algo do formato ```<algo>``` e termina com ```</algo>```. Essas são chamadas de markup tags de abertura e fechamento, respectivamente. Também existem tags que não precisam ser fechadas, como as tags meta do head. Além disso, as tags podem ter atributos, como o markup meta.

O conteúdo a ser exibido está contido dentro do body, como o texto Olá mundo do exemplo. O head contém o título da página (que é exibido na aba do browser), e outras informações, como a codificação dos caracteres e largura da tela (para facilitar o design responsivo). Mais tarde veremos que podemos especificar no head arquivos relacionados à página, como arquivos CSS ou scripts Javascript. Por fim, faltou apenas falar do ```<!DOCTYPE html>```, que é responsável por indicar qual versão HTML estamos lidando, que no caso é a HTML 5.

#### 2.2 Elementos principais e suas Tags

Dentro do corpo, o conteúdo a ser exibido na página deve estar dentro de tags. É importante ressaltar que as markup tags não indicam aspectos de apresentação, apenas possuem significado semântico. Isto é, as tags que você usa devem definir o significado do seu conteúdo o mais próximo possível.

Nesta seção iremos descrever apenas algumas tags, as mais utilizadas. Para uma listagem completa, confira a documentação em https://www.w3.org/TR/html52/.

#### Títulos: ```<h1> - <h6>```

Como mencionado, o arquivo HTML deve refletir a estrutura da página. Uma das principais tags com esse objetivo é a de definição de títulos com nível hierárquico, o que seria equivalente a seções e subseções dessa apostila. São seis tags deste tipo; vamos ilustrar seus comportamentos adicionando as seguintes linhas no corpo do nossa página olamundohtml2/index.html:

...
    <body>
        Olá Mundo
        <h1> Titulo nivel 1 </h1>
        <h2> Titulo nivel 2 </h2>
        <h3> Titulo nivel 3 </h3>
        <h4> Titulo nivel 4 </h4>
        <h5> Titulo nivel 5 </h5>
        <h6> Titulo nivel 6 </h6>
    </body>
...

#### Texto: ```<p>, <br>, <hr>, <strong>, <em>```

Apesar de termos inserido o texto Olá Mundo diretamente no corpo do HTML, em geral isso não é recomendado. Se você reparar, em páginas da internet, um trecho de texto que não é um título é geralmente encapsulado em um parágrafo, o que ajuda na formatação e na construção do layout da página.

Vamos criar um parágrafo com a tag ```<p>``` e inserir o texto Olá Mundo acrescido de mais algumas frases. O novo arquivo HTML deve ficar:

```html
...
    <body>
        <p> Olá Mundo. Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe quae quam veniam cum! Deserunt exercitationem unde maiores dolor magnam autem officiis, iste ab nulla? Facere omnis reiciendis assumenda quod, doloribus, magni maiores quia voluptatum sunt, ad esse. Nam, consectetur voluptatibus? Corporis aliquid hic corrupti culpa doloribus doloremque optio? Hic, quisquam. </p>
        <h1> Título nível 1 </h1>
        ...
    </body>
...
```

ℹ️ No VSCode é capaz de processar a abreviação Emmet loremXXX, onde XXX é o número de palavras, para gerar texto dummy. Para gerar o texto do exemplo, digite lorem50 e pressione enter.

Podemos adicionar quebra de linha no parágrafo com o markup ```<br>``` e linhas horizontais com ```<hr>```. Também podemos atribuir significado a trechos com a tag strong, que denota maior importância, ou em, que dá ênfase de modo a alterar sutilmente o significado da frase.

#### Listas: ```<ol>, <ul>, <li>```

Outro elemento importante em arquivo de texto são listas, que podem ser numeradas ou não. Em HTML, estes dois tipos de listas são representados pelas tags ol e ul, respectivamente. Para facilitar a memorização, entenda como abreviações de ordered list e unordered list. Cada elemento da lista deve ser envolto com o markup ```<li>```. Vamos adicionar algumas lista na nossa página, por exemplo, insira:

```html
...
    <body>
        <ol>
            <li> Item numerado 1 </li>
            <li> Item numerado 2 </li>
            <li> Item numerado 3 </li>
            <li> Item numerado 4 </li>
        </ol>
        <ul>
            <li> Item não ordenado 1 </li>
            <li> Item não ordenado 2 </li>
            <li> Item não ordenado 3 </li>
            <li> Item não ordenado 4 </li>
        </ul>
        <p> Olá Mundo. Lorem ...
    </body>
...
```

#### Tabelas: ```<table>, <tr>, <td>, <th>```

Tabelas podem ser inseridas com a tag table. Elas são descritas pelas linhas ```<tr>```, que contém células ```<td>```. Uma célula pode ser indicada como cabeçalho com o markup ```<th>```. Para facilitar a memorização, as abreviação ```<tr>``` corresponde à table row, ```<td>``` à table data e ```<th>``` à table heading.

Vamos criar uma tabela, insira o seguinte código:

```html
...
    <body>
        <table>
            <tr>
                <th></th>
                <th>Categoria 1</th>
                <th>Categoria 2</th>
                <th>Categoria 3</th>
            </tr>
            <tr>
                <th>Linha 1</th>
                <td>Dado 1_1</td>
                <td>Dado 1_2</td>
                <td>Dado 1_3</td>
            </tr>
            <tr>
                <th>Linha 2</th>
                <td>Dado 2_1</td>
                <td>Dado 2_2</td>
                <td>Dado 2_3</td>
            </tr>
            ...
        </table>
        ...
    </body>
...
```

#### Links: ```<a href="">```

Introduzimos diversas tags, mas ainda não citamos um dos mais importantes elementos em páginas web: os links. Como seu funcionamento deve ser familiar a qualquer um que está lendo esta apostila, vamos pular direto para um exemplo. Na nossa página teste, vamos adicionar:

```html
...
    <body>
        <a href="https://www.poli.usp.br/">Link para o site da Poli</a>
        ...
    </body>
...
```

Como podemos ver, o endereço de destino do link é especificado no atributo href. No exemplo, criamos um link externo. Para criar links para páginas do próprio site, devemos utilizar endereços relativos. Por exemplo, crie o arquivo sobre/autor.html, que é uma nova página para o nosso site. Depois, inicie a estrutura básica do arquivo HTML e insira algum texto. Para inserir um link para esta nova página, no index.html, escreva:

```html
...
    <body>
        <a href="sobre/autor.html">Informações sobre o autor do site</a>
        ...
    </body>
...
```

#### Imagens e Vídeos: ```<img src="" alt="">```

Uma página só com texto não é muito interessante na internet dos dias de hoje. Por isso, vamos aprender como inserir arquivos de imagem e vídeo.

A tag para inserir imagem é a <img> que recebe o endereço da imagem no atributo src e um texto alternativo no atributo alt. É muito importante incluir o atributo alt, tanto para casos em que não foi possível carregar a imagem como para melhorar a acessibilidade do site.

Para adicionar imagens, podemos incluir na nossa página:

```html
...
    <body>
        <img src="images/1.png" alt="Nærøyfjorden, Norway - from Breiskrednosi. UNESCO World Heritage"/>
        <img src="https://source.unsplash.com/320x240/" alt="random unsplash"/>
        ...
    </body>
...
```

Como no caso de links, imagens localizadas no mesmo site são referenciadas de forma relativa. No exemplo, a imagem (https://www.gstatic.com/webp/gallery/1.jpg) foi colocada no diretório images. Também é possível notar que a tag img é do tipo auto-fechamento.

O tamanho das imagens pode ser especificado com os atributos width e height. Esses também podem ser definidos no arquivo CSS, como veremos no próximo capítulo.

Para inserir vídeos, é recomendado adicionar múltiplas fontes, com formatos diferentes. Por exemplo, para um vídeo com os formatos WebM e Mp4, podemos escrever

```html
<video controls width="250">
    <source src="/media/examples/flower.webm" type="video/webm">
    <source src="/media/examples/flower.mp4" type="video/mp4">
    O seu browser nao suporta videos.
</video>
```

Caso nenhum dos dois formatos for aceito na máquina do usuário, a mensagem especificada será exibida.

#### Formulários: ```<form>, <input>, <button>```

Outro aspecto importante de páginas HTML são os formulários, que permitem a entrada de dados pelo usuário. Os formulários também são bastante utilizados para transmitir informações para o back-end.

Os formulários podem ter vários campos de entrada, como nome de usuário, e-mail, senha, etc. Em geral, esses campos são agrupados em uma tag form, do tipo:

```html
<form action="http://www.example.com/subscribe.php" method="get">
    Aqui estarão os controles de formulário
</form>
```

Onde ação e método são combinados para determinar o que ocorrerá quando o formulário for submetido. Não se preocupe com esses campos por enquanto, estes conceitos serão melhor abordados quando discutirmos o protocolo HTTP.

Existem vários tipos de controle, vamos testar apenas alguns deles ao incluir no nosso arquivo index.html:

```html
...
    <body>
        <form>
            <label for="name">Nome:</label>
            <input type="text" id="fname" name="name" placeholder="Coloque o seu nome"><br><br>
            <label for="email">Email:</label>
            <input type="email" id="email" name="email" placeholder="Coloque o seu email"><br><br>
            Gênero: 
            <label for="female"> <input type="radio" name="gender" id="female" value="female"> Fem. </label>
            <label for="male"> <input type="radio" name="gender" id="male" value="male"> Masc. </label>
            <label for="other"> <input type="radio" name="gender" id="other" value="male"> Outro </label><br><br>
            <button type="submit">Submeter</button>
        </form>
        ...
    </body>
...
```

Como pode ser observado, a maior parte dos controles é descrita com a tag input, cujo tipo varia de acordo com o atributo type; Também é prudente inserir labels em cada campo, a fim de melhorar a acessibilidade. Os labels se associam com os controles através do atributo id. O atributo name dos controles é utilizado para compilar as informações a serem submetidas. Por fim, um botão do tipo submit é utilizado para submeter o formulário. Confira a documentação para verificar todos os controles de formulário.

#### 2.3 Elementos de bloco e elementos em linha

Embora os elementos HTML não devem ter aspecto de apresentação, existe uma distinção importante entre elementos de bloco e em linha. Esta distinção será importante quando trabalharmos com layouts em CSS.

O primeiro grupo, de elementos de bloco, sempre aparece em uma nova linha, como exemplo temos os elementos: ```<p>, <table>```, img, listas, entre outros. Já os elementos em linha continuam na mesma linha de seus elementos vizinhos. Os links ```<a>``` e as tags ```<strong>```, emph e label são exemplos de elementos em linha.

#### Agrupamentos: ```<div> e <span>```

Existem dois tipos de elementos que agrupam elementos HTML sem significado semântico pré-definido; são o div e o span. A diferença entre eles é que o primeiro é um elemento de bloco, e, portanto, resulta em uma nova linha, e o segundo é um elemento em linha.

Para mostrar esta diferença, vamos editar o parágrafo no nosso arquivo HTML exemplo:

```html
...
    <body>
        ...
        <div class="paragrafo-com-titulo">
            <p> Olá Mundo. Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe quae quam veniam cum!
                Deserunt exercitationem unde maiores dolor magnam autem officiis, iste ab nulla? Facere omnis reiciendis assumenda quod, doloribus, magni maiores quia voluptatum sunt, ad esse. 
                <span class="frase-especial">Nam, consectetur voluptatibus? </span>Corporis aliquid hic corrupti culpa doloribus doloremque optio? Hic, quisquam. </p>
            <h1> Título nível 1 </h1>
        </div>
        ...
    </body>
...
```

Os elementos de agrupamento geralmente possuem um atributo de identificação, que pode ser do tipo id ou do tipo class. Se você salvar e visualizar no browser, não verificará nenhuma mudança. Então para que servem esses agrupamentos? A resposta virá no próximo capítulo. Mas, para adiantar, eles servem para auxiliar na definição de estilos no CSS. Por exemplo, você pode definir um tamanho de fonte único para todos os elementos agrupados por um ```<div>``` de uma classe específica.

#### 2.4 Como o browser define como exibir um HTML sem especificação de estilo?

Como você deve ter percebido, apesar de mencionado que as markups tags do HTML não possuem características de apresentação (ou estilo), os elementos da nossa página exemplo foram exibidos com estilos diferentes. Por exemplo, o título ```<h1>``` possui uma fonte com tamanho e peso maiores do que o texto dentro de um elemento do tipo ```<p>```. Outro caso é a distinção da exibição entre listas ordenadas ```<ol>``` e não ordenadas ```<ul>```.

Para responder à pergunta, vamos aprender a investigar as páginas! Isto será uma habilidade muito útil daqui para frente.

Geralmente, a depuração do front-end pode ser realizada a partir das ferramentas de desenvolvimento do próprio browser. No Chrome, elas são ativadas pela tecla F12. Com as ferramentas ativadas, para inspecionar um elemento ```<h1>``` tecle Ctrl + Shift + C e clique no texto "Título nível 1" da nossa página. A janela da Fig. mais adiante deve aparecer. Caso contrário tente selecionar manualmente a aba "Styles". No canto inferior direito, estão listadas as características de apresentação padrão para o elemento ```<h1>```. Cada browser possui o seu conjunto de estilos padrão, que são acionados quando o desenvolvedor não definiu o seu estilo em CSS (como será abordado no capítulo seguinte).

#### 2.5 Novas tags estruturais do HTML5

No HTML4, os elementos div eram amplamente utilizados para agrupar regiões do site para definir sua estrutura. O problema dessa abordagem é a falta de semântica desses elementos, além de não existir padronização no nome das classes, e.g. o cabeçalho poderia ser descrito com ```<div class="head">``` ou ```<div class="header">```.

Verificando as classes mais utilizadas, junto com uma estrutura padrão de um site na internet, foram criados os markups.

As regiões de header, footer e aside são auto-explicativas. O markup ```<main>``` deve ser utilizado para conteúdo que é exclusivo da página e não é reaproveitado em outro lugar no mesmo site. Já o elemento section delimita um agrupamento temático, enquanto article deve representar um conteúdo independente, auto-contido. Ambos podem conter seus próprios elementos header e footer.

### 3. Definindo o estilo com CSS (Cascading Style Sheets)

Como mencionado anteriormente, e reforçado algumas vezes, os arquivos HTML não contêm elementos presentacionais. Esta responsabilidade é terceirizada para os arquivos CSS, que descrevem de forma declarativa os atributos de apresentação de cada elemento HTML.

Os browsers já possuem um estilo padrão, como vimos, para algumas tags HTML. O sistema de CSS funciona de modo que novas definições de estilo sobrepõem as antigas, daí o nome "Cascading", inclusive para as definições padrões. Assim, para cada regra, podemos decidir por sobrescrever apenas algumas propriedades, mantendo parte do estilo anterior.

#### 3.1 Utilizando CSS: em linha (evite!), interno (às vezes) e externo (preferível)

Vamos modificar a cor do nosso título ```<h1>``` para vermelho. Existem três formas de especificar essa regra de estilo. O primeiro consiste em adicionar diretamente no elemento:

```html
<h1 style="color:red"> Título nível 1 </h1>
```

Outra opção é inserir as regras no cabeçalho do documento HTML:

```html
<head>
    ...
    <style>
        h1 {
            color: red;
        }
    </style>
</head>
```

Por fim, a opção mais preferível, pois realiza bem a separação entre o conteúdo (HTML) e a estrutura (CSS) é utilizar um arquivo CSS separado. No cabeçalho da sua página adicione:

```html
<head>
    ...
<link rel="stylesheet" type="text/css" href="css/style.css"/>
</head>
```

Depois, crie o arquivo css/style.css com o seguinte conteúdo:

```css
h1 {
    color: red;
}
```

Em qualquer um dos casos, o resultado deve ser o da Fig. abaixo.

#### 3.2 Seletores

A partir do exemplo da seção anterior, podemos fazer a seguinte generalização para regras CSS:

```css
seletor {
    propriedade1: valor1;
    propriedade2: valor2;
    propriedade3: valor3;
}
```

O primeiro seletor válido que observamos foi o nome de um elemento HTML, como o h1, que é chamado de seletor de tipo. Múltiplos seletores podem ser especificados se separados com vírgula, como, por exemplo:

```css
h1, h2, h3 {
    color: red;
}
```

Se o seletor for *, temos um seletor universal, que aplica a todos os elementos da página.

#### Classes e identificadores

Uma forma bastante comum de delimitar as regras CSS é utilizar classes ou identificadores, que são definidos como atributos dos elementos HTML, como, por exemplo:

```html
<p class="caixa"> Lorem ipsum dolor sit amet consectetur </h1>
<header id="cabecalho-principal"> ... </header>
```

Os identificadores são utilizados quando é esperado que só exista um elemento do tipo; para todos os outros casos utilize classes. Para o exemplo dado, podemos escrever as seguintes regras:

```css
/* Para classes caixa*/
.caixa {
    color: red;
}

/* Para elementos do tipo p com classe caixa */
p.caixa {
    color: red;
}

/* Apenas para o identificador cabecalho-principal */
#cabecalho-principal {
    color: red;
}
```

#### Combinadores

Seletores podem especificar relações entre os elementos, existem quatro tipos:

- Descendente: espaço
- Filho: >
- Irmão adjacente: +
- Irmão qualquer: \~

Abaixo apresentamos alguns exemplos de seletores combinadores:

```css
/* Descendente: parágrafos dentro do cabecalho-principal caixa*/
#cabecalho-principal p {
    color: red;
}

/* Filho: links diretamente descendente de li*/
li>a {
    color: red;
}

/* Irmão adjacente: o primeiro paragrafo depois de um h1 */
h1+p {
    color: red;
}

/* Irmão qualquer: para todo paragrafo irmão de h1 */
h1~p {
    color: red;
}
```

#### Pseudo-classes

As pseudo-classes definem atributos para estados especiais de um elemento. A principal utilização visa determinar diferentes estilos para links e botões. Como no caso das seguintes classes:

- hover: ponteiro em cima do elemento
- activate: quando o link ou botão está sendo clicado
- focus: elemento de formulário possui foco

Teste o exemplo a seguir para verificar o seu funcionamento:

```html
...
    <head>
        ...
        <style>
            button.submit:hover {
                background-color: red;
            }
            
            button.submit:active {
                background-color: chocolate;
            }
            
            input.text:focus {
                color: blue;
            }
        </style>
    </head>
    <body>
        <input type="text" class="text">
        <button type="submit" class="submit">Submeter</button>
    </body>
..
```

Outras pseudo-classes importantes se assemelham ao seletor de filho, com o adicional de também delimitarem a posição na sequência. Confira no exemplo abaixo:

```css
/* Aplica apenas ao primeiro filho*/
li:first-child {
    color: red;
}

/* Aplica apenas ao quarto filho*/
li:nth-child(4) {
    color: red;
}

/* Aplica apenas ao último filho*/
li:last-child {
    color: red;
}
```

Seletor de atributos

Com CSS também é possível limitar o escopo do seletor a partir das propriedades. Isso é bastante útil para elementos tipo input, que se diferenciam basicamente pelo atributo type. Confira alguns exemplos:

```css
/* Aplica para todos os paragrafos que definem o atributo classe*/
p[class] {
    color: red;
}

/* Aplica para inputs do tipo texto*/
input[type=text] {
    color: red;
}
```

#### 3.3 Propriedades/Valores CSS principais

Uma vez descritos os seletores, vamos mudar o foco para as propriedades que alteram o estilo dos elementos. Por enquanto não vamos tratar de propriedades que lidam com posicionamento e construção de layout, fica para a próxima seção.

#### Textos: fontes, cores e alinhamento

Para modificar os aspectos da fonte, vamos analisar o seguinte exemplo:

```css
body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: 12px;
    font-weight: bold;
    line-height: 1.4em;
    color: red;
}
```

Os campos são em geral auto-explicativos; as fontes que aparecem primeiro têm preferência, as demais só são selecionadas a medida que o usuário não tenha instalado a fonte preferencial. Por padrão, os links têm aparência diferenciada; para remover essa configuração utilize text-decoration: none.

Outra opção comum é especificar o alinhamento do texto dentro dos elementos, por exemplo:

```css
h1 {
    text-align: left;
}

p {
    text-align: justify;
}

footer {
    text-align: center;
}
```

Modelo de Caixa: tamanho, margem e padding

Cada elemento em HTML pode ser considerado uma caixa, isto é, pode ter borda, margem e padding (vide Fig. abaixo).

Para especificar estas propriedades, observe o seguinte exemplo:

```css
article {
    width: 300px;
    border: 15px solid #F4A896;
    padding: 50px;
    margin: 20px;
    background-color: #358597;
    color: white;
}
```

A cor de fundo (especificada pelo trio RGB em hexadecimal) é dada pelo background- color. O tamanho da caixa pode ser especificado pelas propriedades width e height, assim como em imagens. A Fig. abaixo mostra o resultado deste exemplo.

No exemplo, foram definidas as margens e paddings com valores iguais para todos os lados. Para definir margens individualmente cada um, temos as seguintes opções:

```css
article {
  margin-top: 30px;
  margin-bottom: 10px;
  margin-right: 15px;
  margin-left: 80px;
}

/* O mesmo pode ser obtido com a regra */
article {
  margin: 30px 80px 10px 15px;
}
/* A ordem é: topo, dir., embaixo, esq. */

/* Também é possivel definir valor para os pares topo/embaixo e dir/esq */
article {
  margin: 30px 10px; /* = margin: 30px 10px 30px 10px */
}
```

Os mesmos formatos podem ser adotados para o padding. No caso de margens, o valor auto pode ser atribuído para igualar as margens opostas, o que pode ser útil para centralizar uma caixa.

#### 3.4 Cascateamento e especificidade

No CSS, quando duas regras idênticas, ou com a mesma especificidade, são definidas, a que é declarada por último será usada. Isto é por causa do efeito de cascateamento, que é um dos conceitos importantes do CSS.

Por exemplo, caso sejam definidas as seguintes regras:

```css
h1 { 
    color: red; 
}

h1 { 
    color: blue; 
}
```

A cor do texto do título de nível 1 será azul, pois a última regra que deve ser aplicada.
Especificidade

Mas, e quando duas regras diferentes são aplicáveis para um mesmo elemento? Por exemplo, no CSS:

```css
.card-text { 
    color: blue; 
}

p { 
    color: red; 
}
```

Caso tenhamos um elemento do tipo:

```html
<p class="card-text"> Lorem Ipsum </p>
```

Qual deverá ser a cor do texto?

Para responder esta pergunta, devemos compreender o conceito de especificidade, pois a escolha será sempre para o mais específico. Os seletores são divididos em três tipos:

- Seletores de tipo
- Seletores de classe e pseudo-classes
- Seletores de identificadores

que estão ordenados por nível de especificidade. Assim, por exemplo, um seletor de classe irá substituir um de elemento de parágrafo, que é um seletor de tipo. Consequentemente, no exemplo acima, a cor do parágrafo será azul.

#### 3.5 Variáveis e cálculos

O CSS3 introduziu a possibilidade de definir variáveis. Um bom uso para esta funcionalidade é designar cores que serão reutilizadas recorrentemente, por exemplo:

```css
:root {
    --primary-color: #000000;
    --secondary-color: #ffffff;
    --primary-background: #fbfbfc;
    --secondary-background: #358597 ;
}

article {
    border: 15px solid #F4A896;
    color: var(--secondary-color);
    background-color: var(--secondary-background);
}
```

Você também pode fazer cálculos, inclusive com unidades diferentes. Um exemplo prático é definir um tamanho de uma caixa em relação à tela:

```css
article {
    width: calc(100)
}
```

Neste caso, um elemento article vai ocupar todo o espaço horizontal do pai (não da tela, necessariamente) menos uma "margem" de 30 pixels.

### 4. Trabalhando com leiaute no CSS

A grande hora chegou, com a definição do leiaute podemos finalizar o nosso site! Por padrão, os elementos HTML são posicionados um embaixo do outro, sem nenhuma preocupação especial. Aprendemos que podemos alinhar o texto dentro dos elementos, mas ainda não vimos como gerenciar o posicionamento de elementos, e.g. como centralizar o ```<h1>``` dentro do ```<header>```.

Em CSS, é possível estabelecer posicionamento individual para cada item com a propriedade position, o que pode ser útil apenas em casos específicos. No geral, vamos utilizar duas poderosas funcionalidades: o CSS Flexbox e o CSS Grid.

ℹ️ Se você procurar tutoriais na internet, pode encontrar diversos leiautes criados em CSS com floats. Os floats possibilitam a inserção de texto junto com imagens e, por isso, precisam de algumas adaptações para gerar leiaute. Apesar de funcional até certo ponto, Flexbox e Grid exercem esse papel muito melhor.

#### 4.1 Flexbox

O CSS Flexbox gerencia o posicionamento de itens em uma dimensão, que pode ser horizontal ou vertical. A direção define o eixo principal (paralelo) e o eixo cruzado (perpendicular). Se você já estiver familiarizado com o Qt, pode fazer uma analogia com o QHBoxLayout e o QVBoxLayout.

Assim, considere o exemplo de três elementos ```<div>``` com classes child-1, child-2 e child-3, que estão dentro de outro div de classe container. Para posicionar os três itens no eixo horizontal basta utilizar o seguinte código CSS:

```css
.container {
    border: 10px solid black;
    width: 100;
    height: 360px;
    display: flex;
    flex-direction: row;
}

.child-1 {
    width: 100px;
    height: 100px;
    background: red;
}

.child-2 {
    width: 100px;
    height: 100px;
    background: blue;
}

.child-3 {
    width: 100px;
    height: 100px;
    background: green;
}
```

Ao estabelecer display: flex; em um elementos, estamos controlando o posicionamentos dos seus filhos (descendentes diretos). A propriedade flex-direction controla a direção e o eixo principal. Dependendo deste valor, o posicionamneto resultante pode ser alterado

#### Alinhamento das linhas e colunas

Vamos considerar a direção como linha (flex-direction: row); assim, para o caso de coluna apenas inverta os eixos. Você pode especificar o espaçamento dos itens na horizontal com a propriedade justify-content no div .container.

#### Gerenciando o tamanho dos itens automaticamente

O Flexbox também permite variar o tamanho de um elemento de acordo com o demais. Isso é feito com a propriedade flex-grow, que deve ser atribuída a um dos filhos. Essa propriedade determina o quanto o item pode crescer em relação aos outros. Sinta-se à vontade para experimentar outros valores e ver como o leiaute reage. Quando o flex-grow é especificado, é importante descrever a largura do item com a propriedade flex-basis ao invés de width.

Com essas propriedades, já é possível criar os leiautes pensando em uma dimensão. No entanto, o Flexbox possui mais um truque, ele tem capacidades responsivas também. Isto é, ele pode se adaptar ao tamanho da tela com a propriedade flex-wrap: wrap. O alinhamento entre linhas pode ser controlado com a propriedade align-content de modo similar à propriedade align-items.

#### 4.2 CSS Grid

Enquanto o Flexbox trabalha com um eixo principal, o CSS Grid lida com as duas dimensões para criar um leiaute. Isso possibilita maior controle sobre alinhamento em duas direções. Se você já estiver familiarizado com o Qt, pode fazer uma analogia com o QGridLayout.

Vamos reutilizar o HTML da seção anterior, que possui três itens coloridos dentro de um contêiner. Para transformar nosso contêiner em um CSS Grid, adicionamos a propriedade display: grid. Para definir uma grade, podemos especificar o tamanho de cada coluna e linha com as propriedades grid-template-columns e grid-template-rows.

Um exemplo de leiaute criado com o CSS Grid segue:

```css
.container {
    width: 480px;
    height: 360px;
    border: 10px solid black;
    display: grid;
    grid-template-columns: 100px 100px;
    grid-template-rows: 150px 150px;
}
.child-1 {
    background: red;
}

.child-2 {
    background: blue;
}

.child-3 {
    background: green;
}
```

Onde as dimensões das colunas/linhas podem ser dadas em frações (unidade fr). Para evitar repetições, pode ser utilizada a função repeat(num, dim).

Também é possível incluir espaçamento entre linhas e colunas com:

```css
.container {
    ...
    display: grid;
    ...
    column-gap: 10px;
    row-gap: 10px;
}
```

#### Áreas do CSS Grid

No exemplo anterior, os elementos foram alocados automaticamente na grade, sempre ocupando apenas uma célula. Para construir leiautes mais complexos, no entanto, podemos especificar a posição e o tamanho (em número de células) de cada elemento na grade. Isso pode ser feito pelas propriedades grid-column-start e grid-column-end (serve para row também), que indicam em quais linhas começa e termina o elemento.

Um modo mais conveniente é utilizar áreas, que podem ser delimitadas pela propriedade grid-template-areas no contêiner. Depois, devemos especificar grid-areas para cada filho, como mostra o exemplo:

```css
.container {
    ...
    grid-template-areas: "bx3 bx2"
                         "bx1 bx2";
}

.child-1 {
    background: red;
    grid-area: bx1;
}

.child-2 {
    background: blue;
    grid-area: bx2;
}

.child-3 {
    background: green;
    grid-area: bx3;
}
```

#### Alinhando itens dentro da grade

Assim como no Flexbox, podemos controlar o alinhamento dos itens nas células com as propriedades justify-items (horizontal) e align-items (vertical) no contêiner. Para configurar cada filho individualmente, existem as propriedades justify-self e align-self. 

#### Ajuste automático do número de linhas/colunas

Assim como no Flexbox, você pode delimitar um tamanho mínimo/máximo para o elemento e, assim, deixar o Grid determinar automaticamente o número de linhas/colunas. Isso pode ser feito modificando o comando repeat, como, por exemplo:

```css
.container {
    width: 480px;
    height: 360px;
    border: 10px solid black;
    display: grid;
    grid-template-columns: repeat(auto-fit, minmax(150px, 1fr));
    grid-template-rows: 150px 150px;
}
```

Cujas dimensões mínimas e máximas para cada filho foram especificadas com 150px e 1fr, respectivamente.

ℹ️ Flexbox ou Grid? Essa é resposta é fácil: os dois! Um modo bastante efetivo de combinar as duas ferramentas é utilizar o Grid para realizar o leiaute do site em geral e o Flexbox para ajustar os itens dentro das seções da página.

#### 4.3 Design responsivo

Em sua essência, o design responsivo visa adaptar os elementos de uma página de acordo com o tamanho da tela. Esta necessidade é evidente nos dias de hoje, em que grande parte dos acessos é feita via celular.

Um modo simples de testar se o leiaute de um site é responsivo consiste em redimensionar a janela do browser. Faça isso com a página do exercício do capítulo anterior, você constatará que a exibição em telas pequenas é prejudicada.

#### Dimensões relativas, mínimas e máximas

Uma primeira abordagem para adaptar os elementos para diferentes tamanhos de tela é utilizar dimensões relativas ou variáveis. Para especificar dimensões que podem ser estendidas, é possível utilizar a propriedade min-width ou min-height ao invés de simplesmente usar width ou height, respectivamente.

Ao invés de designar dimensões ou margens fixas com a unidade px, podemos utilizar a unidade rem. Esta é relativa ao tamanho de fonte padrão, que pode ser redefinida no :root. Por exemplo, elementos .box do código abaixo possuem pelo menos 32 pixels de altura.

```css
:root {
    font-size: 16px;
}

.box {
    min-height: 2rem;
}
```

Como já mostrado em exemplos anteriores, é possível utilizar porcentagem para determinar dimensões relativas ao elemento pai. Para dimensões em relação à tela, independente do nível do elemento, podem ser utilizadas as unidades vh e vw para declarar tamanhos em relação à altura e largura do display, respectivamente.

#### Media queries

As propriedades citadas, assim como a capacidade do Flexbox e Grid para controlar automaticamente o tamanho dos elementos filhos, ajudam a melhorar a visualização em telas de tamanhos variados. No entanto, às vezes é desejável controlar o leiaute diretamente de acordo com o tamanho da tela. Por exemplo, podemos modificar a direção de um Flexbox quando o comprimento da tela for menor que 576px com:

```css
.container {
    flex-direction: row;
}
    
@media only screen and (max-width: 576px) {
    .container {
        flex-direction: column;
    }
}
```

Também é possível utilizar min-width para realizar leiautes "mobile first", em que o design principal é feito visando telas de celular.

### 5. Hospedando o site no Gitlab Pages

Até aqui, o site que desenvolvemos só está armazenado no seu computador e, deste modo, não pode ser acessado por outras pessoas. Existem diversas opções gratuitas para hospedar sites estáticos, que não variam o conteúdo a cada acesso; nesta apostila vamos mostrar como utilizar o próprio Gitlab USP para este fim. O serviço de hospedagem de páginas do Gitlab é o Gitlab Pages, que permite hospedagem de sites estáticos, puramente em HTML / CSS ou a partir de geradores de sites estáticos modernos.

ℹ️ O Github também possui um serviço similar, o Github Pages. A sua configuração é ainda mais simples da do Gitlab Pages, e também possui suporte para geradores de sites estáticos.

#### 5.1 Criando o repositório e realizando o push para o Gitlab USP

Siga os procedimentos indicados na apostila de Git para criar um novo repositório no Gitlab USP chamado siteestatico.

⚠️ Por uma limitação do TLS do HTTPS, o Gitlab Pages irá falhar caso o nome do seu usuário possuir o caractere '.' (ponto). Um modo de driblar esta limitação é criar um novo grupo para o seu repositório. No seu dashboard do Gitlab USP, clique em "Your Groups" no canto superior direito e depois em "New group". Não utilize pontos no nome do seu novo grupo. Na página do seu novo grupo, clique em "New project" para criar o seu repositório.

Depois, vamos abrir um terminal e navegar até a pasta que está o arquivo index.html. Note que esta pasta e suas subpastas devem conter apenas o(s) arquivo(s) HTML e o(s) arquivo(s) CSS. Caso contrário, crie uma nova pasta e mova estes arquivos para lá.

Agora, vamos realizar os seguintes passos:

inicialize o repositório local com:

```bash
$ git init
```

realize o commit inicial com:

```bash
$ git add -A
$ git commit -m "Commit inicial"
```

configure a URL repositório remoto com:

```bash
$ git remote add origin https://gitlab.uspdigital.usp.br/fulano/siteestatico.git
```

e, finalmente, faça o push com:

```bash
$ git push -u origin master
```

Todo este processo é explicado em maior detalhes na apostila de Git.

#### 5.2 Configurando o CI/CD para automatizar publicação do site

Para utilizar o Gitlab Pages, é necessário configurar uma pipeline de CI/CD para que, toda vez que realizar o push, o site seja atualizado e a nova versão seja publicada no Gitlab Pages. A configuração é feita a partir do arquivo .gitlab-ci.yml, que deve estar na raiz do seu repositório. Crie um arquivo vazio com este nome, de modo que a estrutura do seu diretório seja algo parecido com:

```markdown
📂siteestatico
 ┣ 📂css
 ┃ ┗ 📜style.css
 ┣ 📜.gitlab-ci.yml
 ┗ 📜index.html
 ```

Agora, abra o arquivo .gitlab-ci.yml em um editor de texto (ou o VSCode) e insira o seguinte conteúdo:

```yml
image: alpine:latest
pages:
 script:
   - mkdir .public
   - cp -r * .public
   - mv .public public
 artifacts:
   paths:
     - public
 only:
   - master
```

Para concluir a configuração, faça um novo commit com esse arquivo e realize o push:

```bash
$ git add .gitlab-ci.yml
$ git commit -m "Criar arquivo de configuração CI/CD"
$ git push
```

#### 5.3 Verificando o status do pipeline CI/CD

Assim que o push é realizado, será acionado automaticamente o processo de publicação do site. Para verificar o seu status, vá em "CI/CD" ➡️ "Pipelines" no menu lateral esquerdo.

#### 5.4 Acessando sua página

Após alguns minutos, a sua página deve estar acessível em:

https://usuario.e.usp.br/siteestatico

onde usuario deve ser substituído pelo seu nome de usuário do Gitlab USP.

Você também pode confirmar ou obter este endereço na opção "Settings" ➡️ "Pages" no menu lateral esquerdo da página do seu repositório

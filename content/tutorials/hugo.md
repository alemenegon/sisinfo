---
title: "Tutorial de HUGO"
date: 2023-09-28T21:27:57-03:00
draft: false
---

## Aprenda a usar HUGO!

{{< rawhtml >}}
<div class="image">
    <img src="/sisinfo/images/hugo.jpeg" alt="logo"> 
</div>
{{< /rawhtml >}}

### 1. Hugo, gerador de site estático

Sites estáticos consistem basicamente em páginas HTML, com arquivos CSS para definir estilos e scripts Javascript. Não possuem interação com banco de dados nem com outros serviços.

Predominantes no primórdio da internet, foram perdendo espaço na medida que ferramentas foram desenvolvidas para simplificar o processo de criação de sites dinâmicos. No entanto, atualmente existe uma tendência de volta desses sites, quando aplicável. Sites estáticos são ideais para os casos de blog, portfólio pessoal, notícia e divulgação de conteúdos como tutoriais, artigos, vídeos ou podcasts.

A vantagem de sites estáticos é a sua eficiência, é muito mais rápida a navegação, uma vez que apenas é necessário obter e processar a página HTML. Ademais, a manutenção e hospedagem de sites estáticos em geral é bastante simples.

Desse modo, ferramentas para geração de sites estáticos estão em alta. Neste exercício, propomos a utilização do Hugo para gerar um site estático.

#### 1.1 Instalação

Siga as instruções em https://gohugo.io/getting-started/installing/ para instalar no Windows, Mac ou Linux.
Instalando com o Chocolatey no Windows

A opção de instalação mais recomendada para o Windows é com o Chocolatey, cuja instalação foi descrita na apostila de Git. Com o Chocolatey instalado, abra uma janela do powershell e execute:

```bash
$ choco install hugo
```

#### Verificando a instalação

Para verificar que a instalação foi bem sucedida, abra uma janela do seu terminal e execute o comando abaixo, que deve exibir uma saída similar.

```bash
$ hugo version
hugo v0.88.1-5BC54738 windows/amd64 BuildDate=2021-09-04T09:39:19Z VendorInfo=gohugoio
```

A próxima seção descreve o uso básico do Hugo; caso deseje pular direto para o enunciado do exercício, prossiga para a seção 4.

### 2. Utilizando o Hugo

No Hugo, assim como em outros softwares de geração de sites estáticos, se faz a separação entre o leiaute e o conteúdo. O primeiro define o design das páginas e, para o segundo, resta apenas definir o conteúdo principal. Isso possibilita uma maior produtividade na atualização do site estático e aumenta bastante o reaproveitamento de código.

O Hugo cria o site sem a configuração de nenhum leiaute; a partir disso, temos duas opções: aproveitar um tema pronto ou definir os nossos próprios leiautes com HTML e CSS. Para esta atividade vamos optar pelo segundo caminho, a fim de exercitar os conhecimentos de linguagens de front-end.

#### 2.1 Criando um site e investigando a estrutura do diretório

O Hugo, assim como muitas ferramentas de desenvolvimento Web, é operada através da linha de comando. Assim, para criar um novo site, abra o terminal, navegue até uma pasta de usuário, e execute

```bash
$ hugo new site nomedosite
```

onde nomedosite deve ser substituído pelo nome do seu site.

Isto deve criar o diretório nomedosite com as seguintes pastas e arquivos

```markdown
📦nomedosite
┣ 📂archetypes
┃ ┗ 📜default.md
┣ 📂content
┣ 📂data
┣ 📂layouts
┣ 📂static
┣ 📂themes
┗ 📜hugo.toml
```

Nesta apostila, iremos nos concentrar nas pastas layouts, que conterá os leiautes, e content, que abrigará os arquivos de conteúdo. Também modificaremos algumas configurações através do arquivo hugo.toml.

Para usar o VSCode na edição do site, execute

```bash
$ cd nomedosite
$ code .
```

Note que, a partir de agora, todos os comandos do terminal devem ser executados a partir do diretório atual, que é a raiz do projeto.

Aproveite para inicializar o seu repositório Git e fazer o primeiro commit com

```bash
$ git init
$ git add -A
$ git commit -m "Primeiro commit"
```

ou através da interface gráfica do VSCode.

#### 2.2 Tipos de leiaute

O Hugo requer a definição de pelo menos três tipos de leiautes: da página principal (home page layout), de páginas de conteúdo (single page layout) e da página de listagem (list layout). Por exemplo, em um site de uma loja, as páginas poderiam ser as seguintes:

- principal, que usa o home page layout;
- produto #1, que usa o single page layout;
- produto #2, que usa o single page layout;
- contato, que usa o single page layout;
- sobre, que usa o single page layout;
- vitrine da loja, que usa o list layout.

Nesta seção, vamos criar apenas a página principal e a página "sobre". Assim, precisamos definir os leiautes de página principal e de conteúdo.

#### 2.3 Criando a página inicial

O home page layout é um arquivo de template Go, que deve ser criado em layouts/index.html. Um exemplo de leiaute de página principal é

```html
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ .Site.Title }}</title>
    </head>

    <body>
        <h1>{{ .Site.Title}}</h1>
        {{ .Content }}
    </body>

</html>
```

onde você pode observar a semelhança com a linguagem HTML. As exceções são as partes envoltas com dupla chaves, que se utilizam da linguagem de template.

A principal diferença aqui é que, para inserir dados provenientes do arquivo de conteúdo, são usadas as variáveis do Hugo. A linha {{ .Content }} exibe o conteúdo da página, que é definido em um arquivo na pasta content. Além do conteúdo, páginas individuais também disponibilizam o título em .Title e as informações gerais do site na variável .Site. Por este motivo, conseguimos exibir o título do site com {{ .Site.Title }}. A listagem completa de variáveis pode ser conferida na documentação oficial em https://gohugo.io/variables/page/.

ℹ️ o título do site pode ser configurado no arquivo hugo.toml

Como explicado anteriormente, o Hugo separa o leiaute do conteúdo. Assim, como já definimos o leiaute, falta apenas criar o conteúdo; no caso da página principal, ele é definido em content/_index.md. Crie este arquivo e inclua o seguinte conteúdo para teste

```markdown
Este é o meu portfólio

Neste site, você encontrará

- Minha biografia
- Meus projetos
- Meu currículo
```

ℹ️ o markdown é um formato minimalista para formatar documentos de texto simples. Sua especificação pode ser resumida em uma breve página, como em https://www.markdownguide.org/cheat-sheet/.

Com isso, o Hugo já pode gerar nossa página principal. Para facilitar o desenvolvimento, podemos usar o servidor de desenvolvimento do Hugo. Ele tem funcionalidades similares à extensão "Live Server" do VSCode, com atualizações automáticas. Para iniciá-lo, execute no terminal

```bash
$ hugo server
Start building sites … 
hugo v0.88.1-5BC54738 windows/amd64 BuildDate=2021-09-04T09:39:19Z VendorInfo=gohugoio
...
Running in Fast Render Mode. For full rebuilds on change: hugo server --disableFastRender
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```

Com isso, abra o browser e vá em http://localhost:1313 para visualizar a página principal.

⚠️ Caso a saída do comando indique uma porta diferente de 1313 (denotada pelo número depois dos dois pontos), modifique o endereço no browser de acordo.

⚠️ Caso suas mudanças não sejam atualizadas, execute o comando hugo server --disableFastRender em vez de hugo server.

#### 2.4 Criando a página "sobre"

Para uma página individual que não seja a principal, o leiaute a ser usado é do tipo single. Então, para criar a página "sobre", precisamos antes definir este leiaute em layouts/_default/single.html. Um exemplo seria

```html
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>{{ .Site.Title }}</title>
    </head>

    <body>
        <h1>{{ .Site.Title}}</h1>
        <h2>{{ .Title }}</h2>
        {{ .Content }}
    </body>

<html>
```

cuja única diferença para o template da seção anterior é a exibição do título da página.

Agora, para criar o conteúdo, podemos usar a interface de linha de comando do Hugo; no terminal, execute

```markdown
$ hugo new about.md
```

que deve criar o arquivo content/about.md, já preenchido com

```markdown
---
title: "About"
date: 2021-09-18T16:59:34-03:00
draft: true
---
```

O trecho delimitado pelas linhas ---, que é denominado de front matter, contém dados a respeito da página e não é incluído no HTML. Caso a propriedade draft seja true, o Hugo ignorará este arquivo na geração final do site (mas não no hugo server). Portanto, vamos modificar esta propriedade e atualizar o arquivo para

```markdown
---
title: "About"
date: 2021-09-18T16:59:34-03:00
draft: false
---
```

Esta página contém informações sobre minha humilde pessoa.

Inicie mais uma vez o servidor com

```bash
$ hugo server
```

Para ver a nova página em http://localhost:1313/about.

#### 2.5 Hyperlinks: adicionando um menu de navegação

Para adicionar um menu de navegação, modifique os arquivos de leiautes criados layouts/index.html e layouts/_default/single.html

```html
...
<body>
    <header>
        <h1>{{ .Site.Title}}</h1>
        <nav>
            <ul>
                <li><a href="{{ relURL "" }}">Home</a></li>
                <li><a href="{{ relURL "about" }}">Sobre</a></li>
            </ul>
        </nav>
    </header>
...
```

onde utilizamos a função relURL para gerar as URLs relativas à raiz do site. Outras funções disponibilizadas pela linguagem de template podem ser consultadas em https://gohugo.io/categories/functions. É importante evitar o uso direto das URLs, pois os links podem não funcionar quando hospedados em um domínio com um endereço base diferente.

#### 2.6 Usando blocos e partials (opcional)

Como pode ser visto até agora, todas as páginas compartilham um esqueleto HTML padrão. Sabendo disso, o Hugo fornece um lugar único para colocar esse esqueleto, que será compartilhado entre todos os leiautes.

Para utilizá-lo, crie o arquivo layouts/_default/baseof.html como o conteúdo

```html
<html lang="en">

    {{ partial "head.html" . }}

    <body>
        {{ partial "header.html" . }}
        <main>
            {{ block "main" . }}
            {{ end }}
        </main>
        {{ partial "footer.html" . }}
    </body>

</html>
```

onde são utilizadas as funções partial e block.

A função partial importa o conteúdo de outros arquivos, para possibilitar o reuso desses códigos. Com isso, precisamos criar os arquivos layouts/partials/head.html, layouts/partials/header.html e layouts/partials/footer.html.

ℹ️ O ponto após o nome do arquivo indica que estamos passando as variáveis do escopo atual para esses partials. Assim, poderemos utilizá-las para recuperar o título e o conteúdo da página.

Crie o arquivo layouts/partials/head.html com o conteúdo

```html
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ .Site.Title }}</title>
</head>
```

o arquivo layouts/partials/header.html com o conteúdo

```html
<header>
    <h1>{{ .Site.Title}}h1>
    <nav>
        <ul>
            <li><a href="{{ relURL "" }}">Home</a></li>
            <li><a href="{{ relURL "about" }}">Sobre</a></li>
        </ul>
    </nav>
</header>
```

e, por fim, o arquivo layouts/partials/footer.html com o conteúdo

```html
<footer>
    <p>Copyright 2021 Me.</p>
</footer>
```

Para utilizar este template base, os leiautes existentes necessitam apenas definir o bloco main. Este bloco irá substituir as linhas

```html
{{ block "main" . }}
{{ end }}
```

encontradas no arquivo layouts/_default/baseof.html.

ℹ️ Caso algum conteúdo fosse definido dentro do bloco no arquivo base, este serviria como o conteúdo padrão. Assim, ele só seria exibido caso o bloco não fosse redefinido.

Para definir um bloco, iremos utilizar a função define. Desse modo, podemos reescrever o leiaute layouts/index.html como

```html
{{ define "main" }}
    {{ .Content }}
{{ end }}
```

e o leiaute layouts/_default/single.html como

```html
{{ define "main" }}
    <h2>{{ .Title }}</h2>
    {{ .Content }}
{{ end }}
```

#### 2.7 Criando o leiaute de página de listagem

No Hugo, o conteúdo pode ser organizado em subdiretórios, geralmente separados por assuntos. Sabendo disso, podemos criar as páginas de tutorial com o comando

```bash
$ hugo new tutorials/django.md
$ hugo new tutorials/flask.md
```

Depois, modifique tutorials/django.md para

```markdown
---
title: "Django"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Aprenda Django em apenas quatro passos!

1. Step one
2. Step two
3. ????
4. PROFIT!!!
```

e tutorials/flask.md para

```markdown
---
title: "Flask"
date: 2021-09-18T23:28:40-03:00
draft: false
---

Aprenda Flask em apenas quatro passos!

1. Step one
2. Step two
3. ????
4. PROFIT!!!
```

Como esperado, ao iniciar o servidor e navegar para http://localhost:1313/tutorials/django/ ou http://localhost:1313/tutorials/flask/, você deverá obter as páginas criadas com o leiaute definido.

Além dessas páginas, o Hugo também cria automaticamente uma página com a lista de todos os tutoriais. Para exibi-las, no entanto, precisamos primeiro criar o leiaute deste tipo de páginas. Para tal, crie o arquivo layouts/_default/list.html com o conteúdo

```html
{{ define "main" }}
<h2>{{ .Title }}</h2>
<ul>
    {{ range .Pages }}
        <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
{{ end }}
```

onde podemos observar que temos a variável .Pages, contendo a lista de páginas, à disposição. Para iterar a lista, é usada a função range, que dá acesso às mesmas variáveis do leiaute de single pages, incluindo .RelPermalink, que retorna a URL relativa.

ℹ️ Você pode criar leiautes específicos para cada seção do seu site. Por exemplo, você pode definir leiautes específicos para tutoriais nos arquivos layouts/tutorials/single.html (para páginas únicas) e layouts/tutorials/list.html (para páginas de listagem). Também é possível configurar leiautes específicos para conteúdos alterando a propriedade layout do front matter.

Para finalizar, podemos adicionar um link para os tutoriais na barra de navegação; em layouts/partials/header.html, adicione

```html
...
<ul>
    ...
    <li><a href="{{ relURL "tutorials" }}">Tutoriais</a></li>
</ul>
...
```

#### 2.8 Trabalhando com arquivos estáticos

Além dos arquivos de leiaute e conteúdo, é possível adicionar imagens, folhas de estilo CSS e/ou scripts JS. Para esses casos, você pode utilizar a pasta static; qualquer arquivo armazenado nela é visto como se estivesse na raiz do projeto.

Assim, vamos adicionar uma imagem; crie o arquivo static/images/logo.svg e insira o conteúdo

```html
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="64" height="64">
    <rect x="0" y="0" height="64" width="64" style="stroke:#ff0000; fill: #0000ff"/>
</svg>
```

Em seguida, crie o arquivo static/css/style.css e preencha com

```css
header {
  display: flex;
  justify-content: space-between;
  width: 80%;
  margin: 0 auto;
}

main {
  width: 80%;
  margin: 0 auto;
  flex: 1;
}

footer {
  display: flex;
  justify-content: center;
}

body {
  height: 100vh;
  display: flex;
  flex-direction: column;
}
```

Para adicionar a imagem do logo, inclua a seguinte linha em layouts/partials/header.html

```html
<header>
    <img src="{{ relURL "images/logo.svg" }}" alt="logo"> 
    <h1>{{ .Site.Title}}</h1>
    ...
```

E, para incluir o CSS em todas as páginas, adicione no partial layouts/partials/head.html

```html
...
    <link rel="stylesheet" href="{{ relURL "css/style.css" }}"> 
    <title>{{ .Site.Title }}</title>
</head>
```

Reinicie o servidor de desenvolvimento e veja que as mudanças surtiram o efeito desejado.
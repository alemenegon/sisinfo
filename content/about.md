---
title: "Sobre"
date: 2021-09-18T16:59:34-03:00
draft: false
---

## Introdução
Sistemas de Informação são conjuntos organizados de pessoas, hardware, software, redes de comunicação e recursos de dados que coletam, processam, armazenam e distribuem informações para apoiar a tomada de decisões e o controle nas organizações. Bancos de Dados, por sua vez, representam coleções organizadas de informações armazenadas eletronicamente em um computador, permitindo o fácil acesso, gerenciamento e atualização de dados de diversos tipos. 

O Front-end refere-se à parte visível de um aplicativo ou site, incluindo o design, layout, cores e elementos interativos com os quais o usuário interage diretamente. Por outro lado, o Back-end compreende a parte do sistema que trata do processamento de dados e da lógica de negócios, envolvendo o gerenciamento de bancos de dados, servidores e a lógica subjacente ao funcionamento do sistema. 

A Interface do Usuário (UI) engloba todos os elementos visuais e interativos que compõem a interação entre o usuário e o sistema, enquanto a Experiência do Usuário (UX) foca na experiência global do usuário ao interagir com o sistema, incluindo a facilidade de uso e a satisfação geral. Por fim, o Desenvolvimento Ágil é uma abordagem iterativa que busca responder rapidamente às mudanças e às necessidades dos clientes, promovendo a colaboração constante entre equipes e a entrega regular de incrementos de software.

## Requisitos de Software
Sistemas de software são reconhecidamente importantes ativos estratégicos para diversas organizações. Uma vez que tais sistemas, em especial os sistemas de informação, têm um papel vital no apoio aos processos de negócio das organizações, é fundamental que os sistemas funcionem de acordo com os requisitos estabelecidos. Neste contexto, uma importante tarefa no desenvolvimento de software é a identificação e o entendimento dos requisitos dos negócios que os sistemas vão apoiar.

A norma IEEE-90, define como sendo:
- Uma capacidade que um usuário necessita para resolver um problema ou atingir um objetivo;
- Uma capacidade que deve ser atendida ou possuída por um sistema ou componente de um sistema para satisfazer um contrato, padrão, especificação ou outro documento formalmente imposto;
- O conjunto de todos os requisitos que formam a base para o desenvolvimento subseqüente de um software ou componentes de um software. Em outras palavras, quando falamos de requisito, tange a solicitação do cliente, como: necessidades, exigências e desejos.

## Engenharia de Requisitos
### Definição
- A Engenharia de Requisitos estabelece o processo de definição de requisitos.
- Este processo deve lidar com diferentes pontos de vista, e usar uma combinação de métodos, ferramentas e pessoal.
- O produto desse processo é um modelo, do qual um documento de requisitos é produzido.
- Este processo acontece num contexto previamente definido a que chamamos de Universo de Informação

### Atividades
Para um bom desenvolvimento é importante identificar os requisitos, pois a partir desta fase podem surgir muitos erros, que se não corrigidos a tempo impactarão no desenvolvimento.
“No desenvolvimento de software, a qualidade do produto está diretamente relacionada à qualidade do processo de desenvolvimento.”

### Eliciação
ELICITAR = Eliciar + Clarear + Extrair + Descobrir , tornar explícito, obter o máximo de informação para o conhecimento do objeto em questão. Pode envolver:
- Usuários finais
- Gerentes
- Engenheiros envolvidos na manutenção
- Especialistas de domínio
- Estes são chamados stakeholders.

Stakeholder, é um dos termos utilizados em diversas áreas como gestão de projetos, comunicação social, administração e arquitetura de software referente às partes interessadas que devem estar de acordo com as práticas de governança corporativa executadas pela empresa.

### Especificação de Requisitos
Na especificação pode conter requisitos funcionais e
não-funcional e até mesmo um diagrama de caso de uso ou
prototipação de parte do produto. São descritos o passo a
passo de cada funcionalidade bem como suas devidas
restrições.
Extração de requisitos é o processo de transformação das
idéias que estão na mente dos clientes (a entrada) em um
documento formal (saída)
A meta é o reconhecimento dos elementos básicos do
problema, conforme percebidos pelo cliente.

Temos três atividades principais: identificação de fontes de
informação, coleta de fatos e validação dos requisitos.

### Identificação de Fontes de Informação
Universo de Informação: contém toda informação necessária.
- Agentes / Stakeholders (Atores, Usuários)
- Outras fontes de Informação: Documentação do
macrosistema, Políticas, Manuais, Memos, Atas, Contratos,
Livros sobre o tema relacionado, Outros sistemas da
empresa, Outros sistemas externos, etc.
Importante: Priorizar as Fontes de Informação! Atores mais
importantes, Documentos mais mencionados, Rede de
comunicações entre os componentes do macro-sistema.

### Coleta de Fatos
Deve ocorrer por meio de: Leitura de documentos, Observação,
Entrevistas, Questionários, Análise de Protocolos, Participação
ativa dos stakeholders, Reuniões, Reutilização, Recuperação
(engenharia reversa) do projeto do software.
Os entrevistadores devem ter mente aberta, desejarem ouvir
os stakeholders e não ter idéias preconcebidas sobre os
requisitos. Eles devem induzir os entrevistados com uma
questão ou uma proposta, e não simplesmente esperar que
eles respondam a uma questão tal como “o que você quer?”

### Validação dos Requisitos
Atividade fundamental para que a fase de elicitação tenha
sucesso. Trata-se da comunicação entre clientes/agentes e os
engenheiros de software. Após a escrita de necessidades do
cliente/usuário é importante que você valide esses dados, seja
através de uma reunião fazendo com que os responsáveis
assinem o documento para que ele possa ter validade, ou
dando ciência da responsabilidade dos requisitos levantados.
Essa etapa também serve para correções e podem ser
descobertas/inclusas outras funcionalidades.
Entendimento: estabelecimento de um contexto comum.

### Documento de Requisitos
Imagine o desenvolvimento de um sistema que você ou outra
pessoa fizeram há alguns anos atrás, detalhe sem documentar.
Você é capaz de lembrar todas as regras e funcionalidades?
Bom, eu não me recordo de tudo! O documento serve para
apresentar uma visão geral e funcional do produto, mas o que
deve conter?
1. Introdução; 
2. Visão geral do produto; 
3. Termos técnicos específicos para um determinado contexto; 
4. Abreviações e acrônimos; 
5. Envolvidos e Usuários; 
6. Requisitos (Funcionais, Não-Funcionais e Regras de Negócio); 
7. Caso de Uso; 
8. Anexos (protótipos, arquitetura e documentos auxiliares).

### Especificação de Requisitos
Na especificação pode conter requisitos funcionais e
não-funcional e até mesmo um diagrama de caso de uso ou
prototipação de parte do produto. São descritos o passo a
passo de cada funcionalidade bem como suas devidas
restrições.

## Tipos de Requisitos
### Introdução
Além desses, existem também os requisitos de projeto e os
requisitos de processo. Os requisitos podem ser
subclassificados em requisitos funcionais, requisitos de
qualidade ou não funcionais e restrições ou regras de negócio.

### Requisitos Funcionais
São declarações de serviços (ou funcionalidades) que o sistema deve
fornecer, como o sistema deve responder a determinadas entradas e
como o sistema deve se comportar em determinadas situações.
Podem ser descritos tanto no nível de usuário ou de sistema. Os
requisitos funcionais descrevem o quê o sistema deve fazer. É
importante que não haja ambiguidade nem excesso de generalidade.
Também é importante que o levantamento dos requisitos funcionais
seja completo2 e consistente3. Somente após o levantamento de
todo o conjunto de requisitos funcionais se terá uma ideia do escopo
do projeto. Um requisito funcional não pode contradizer nem
inviabilizar outro.
Completo significa que todos os requisitos do software devem ser levantados.
Consistente significa que não pode haver conflitos entre os requisitos.

### Requisitos Não-Funcionais
São restrições às funcionalidades do sistema como, por exemplo,
restrições de tempo, disponibilidade, processo de desenvolvimento,
desempenho do software, plataforma tecnológica, restrições legais,
integração com outros sistemas, segurança da informação, etc.
Geralmente, os requisitos não-funcionais são transversais, isto é,
permeiam o sistema como um todo. Não se referem a uma
funcionalidade específica.
Uma falha no levantamento de requisitos funcionais pode ser
contornável. O usuário pode usar uma solução de contorno para
extrair do software a informação que precisa. Já uma falha no
levantamento de um requisito não funcional normalmente é
incontornável e torna o sistema como um todo inviável.

Considere que no início de um projeto não tenha ficado claro que um
software de contabilidade a ser desenvolvido para um cliente deve
ter o requisito de transmitir dados para o sistema SPED Contábil da
Receita Federal.
Neste caso são dois requisitos não-funcionais em um: a integração
entre os sistemas contábil e o SPED e a obrigação legal de reportar
os dados para a autoridade tributária.
A falha no levantamento desses requisitos é incontornável e
inviabiliza o software como um todo. Ninguém vai querer usar o
novo sistema contábil se ele não se integra com o SPED. Todos vão
querer permanecer no software contábil antigo.

Imagine que, durante um projeto de desenvolvimento de software
para um banco, o cliente tenha deixado claro que a base de dados
deve ser protegida por questões de sigilo bancário. O cliente
transmitiu o requisito no nível de usuário, em linguagem natural.
Uma boa gerência de requisitos irá garantir que esse requisito
não-funcional no nível de usuário seja especificado como um
requisito não-funcional de sistema de que a base de dados do
software deverá estar criptografada com criptografia forte.
Trata-se de um requisito não funcional de segurança da informação.
Uma falha na identificação desse requisito pode inviabilizar o
software como um todo, independentemente de ele atender todos
os requisitos funcionais desejados pelo cliente.

- Requisitos de Produto: São restrições pertinentes ao produto
(software). Deve ser amigável ao usuário? Deve ter um bom
desempenho? Quanto espaço em disco pode utilizar? Depende
de outros sistemas? Precisa de criptografia? Login e senha para
entrar?
- Requisitos Organizacionais: São restrições impostas pelos
processos, políticas ou culturas organizacionais do cliente ou do
desenvolvedor. Deve obedecer ao padrão visual do cliente? O
desenvolvedor só domina determinada tecnologia ou processo
de desenvolvimento?
- Requisitos Externos: São restrições impostas por fatores
externos ao cliente e ao desenvolvedor. Deve ser atendida
alguma lei? Algum normativo interno? Algum código de ética?

### Regras de Negócio
A diferença entre requisito funcional e regra de negócio, conceitualmente
falando, é que o requisito funcional refere-se ao que o sistema
deverá fazer, enquanto a Regra de Negócio refere-se a como o
sistema deverá fazer. Do ponto de vista do negócio (negócio do
cliente para o qual o sistema está sendo feito), ambos são
necessidades (requisito funcional e Regra de Negócio), mas cada
uma com um foco diferente. Apresentar pelo menos cinco
requisitos funcionais e dois requisitos não funcionais.

## Prototipagem
### Benefícios
- Equívocos entre os usuários de software e
desenvolvedores são expostos.
- Serviços esquecidos podem ser detectados e serviços
confusos podem ser identificados.
- Um sistema funcionando está disponível nos primeiros
estágios no processo de desenvolvimento.
- O protótipo pode servir como uma base para derivar uma
especificação do sistema com qualidade de produção.
- O protótipo pode ser usado para treinamento do usuário e
teste de sistema.

### Prototipação Evolucionária
- O processo de especificação, projeto e implementação são
intercalados.
- O sistema é desenvolvido em uma série de estágios que
são entregues ao cliente.
- Técnicas para o desenvolvimento rápido de sistemas, tais
como ferramentas CASE e linguagens apropriadas (como
Ruby on Rails), são utilizadas.
- As interfaces com o usuário do sistema são usualmente
desenvolvidas utilizando-se um sistema de
desenvolvimento interativo.

## Casos de Uso
### Definição
- Unidade funcional provida pelo sistema, subsistema, ou
classe manifestada por sequências de mensagens entre o
sistema e um ou mais atores.
- Representa uma possível utilização do sistema por um
ator, que pode ser uma pessoa, dispositivo físico,
mecanismo ou subsistema que interage com o sistema
alvo, utilizando algum de seus serviços
- O objetivo dos casos de uso é a compreensão do
comportamento externo do sistema por qualquer
stakeholder.
- Um caso de uso narra a interação entre o sistema e os
atores envolvidos, para atingir um ou mais objetivos.
- Deve estar relacionado a um processo bem definido, com
começo, meio e fim:
- Exemplos: Emprestar Livro, Vender Produtos, Incluir ordem
de serviço.

### Componentes
- Atores: Quem executa a funcionalidade;
- Casos de Uso: Qual é a funcionalidade;
- Relacionamentos: Como atores e casos de uso se
relacionam.

### Atores
- Representam os papéis desempenhados pelos diversos
usuários: Cliente, Caixa do Banco, Gerente, etc.
- Atores podem ser: 1. Pessoas que interagem com o
sistema; 2. Um hardware que dispara uma interação; 3.
Outro software que comunica com o sistema;
- O ator é algo (usuário, software ou hardware) que não faz
parte do sistema mas que interage com ele em algum
momento;
- Representação: Homem Palito + Papel Desempenhado.

### Casos de Uso
- Casos de Uso descrevem interações entre o sistema e os
atores;
- Definem os serviços, tarefas ou funções do sistema. Os
nomes indicam ação (verbos): 1. Cadastrar venda: loja; 2.
Sacar :banco; 3. Consultar um filme :locadora.
- Representados por elipses: Um texto dentro descreve a
funcionalidade do caso de uso;
- Geralmente a descrição dentro da elipse é curta e direta:
Verbo [ + Objeto ].

### Relacionamentos
- Principais tipos de relacionamentos: Associação, Inclusão,
Extensão e Generalização.
- Representam as interações entre: Atores e Casos de Uso,
Dois ou mais Casos de Uso, Dois ou mais Atores.
- Associação Ator e Caso de Uso: Demonstra que o Ator
utiliza a função do sistema representada pelo Caso de Uso.
Requisitando a execução da função. Recebendo o
resultado produzido pela função. Representada por uma
reta ligando o Ator ao Caso de Uso, Direcionada ou não.
- Generalização: Acontece quando dois ou mais casos de
uso possuem características semelhantes: Foco em
reutilização.
- Inclusão: Utilizado quando um caso de uso é usado dentro
de outro caso de uso. Os relacionamentos de inclusão
indicam obrigatoriedade. Representada por uma seta
tracejada, a seta aponta para o Caso de Uso incluído,
possui a palavra “include” entre « e ».
- Extensão: Geralmente usado em funcionalidades opcionais
de um caso de uso. Cenários que somente acontecerão em
uma situação específica, se uma determinada situação for
satisfeita. Semelhante à Inclusão, A palavra “extend” entre
« e ».

## Casos de Uso Textuais
### Conteúdo
- Breve Descrição: descrever, de forma sucinta, o objetivo do
caso de uso, ou seja, a função do sistema que é
implementada. Nos casos de uso mais simples (por
exemplo, incluir produto), acaba ficando meio parecido
com o nome. Apresentar uma prototipação do caso de uso.
Devem ser indicados todos os campos a serem obtidos ou
exibidos, incluindo o tipo de dado e todas as regras de
validação.
- Atores: listar os atores que podem se utilizar deste caso de
uso, com por exemplo: operacional do estoque, gerente de
contas, responsável pelo envio de material, etc. Nunca
usar termos genéricos como “usuário”.

### Pré-Condições
Listar as pré-condições para que este caso
de uso seja executado, como por exemplo: ator deve estar
logado no sistema e ter permissão para acesso à função.
Algumas vezes, quando se combina que esta condição de
ator logado é default, pode-se omiti-la. Se não houverem
pré-condições, escrever simplesmente “Não se aplica” ou
“Nenhuma pré-condição especial é requerida” ou algo
equivalente.

### Pós-Condições
Pós-Condições: lista das pós-condições, ou seja, do
resultado do processamento do caso de uso. Muitas vezes,
fica óbvio; por exemplo, na inclusão de um produto, a
pós-condição é que o produto foi incluído. Mesmo assim,
escreva!

### Fluxo Básico
Descrever o fluxo básico do processamento. O fluxo deve
ser linear, ou seja, não podem haver “if-then-else” nem
“prosseguir para o passo xx”. Se houverem condições como
esta, devem ser explicitadas em fluxos alternativos. O fluxo
normalmente consta de um diálogo usuário-máquina,
envolvendo frases como “o ator faz alguma coisa” e “o
sistema faz algo”.

### Fluxos Alternativos
Os fluxos alternativos descrevem situações de erro do
fluxo principal ou formas alternativas do usuário cumprir a
mesma funcionalidade. Por exemplo, para salvar um
arquivo no Word, pode-se usar o menu File-Save ou o
atalho control-S. A descrição segue os mesmos padrões
que o fluxo principal, inclusive com exemplos de telas,
quando for o caso. Se não houverem fluxos alternativos,
não remova o item; escreva simplesmente “Não aplicável”.

### Requerimentos Especiais
Utiliza-se para a descrição do algoritmo, para apontar
alguma situação especial (por exemplo, o tempo de
resposta ou o número mínimo de valores a serem exibidos
em cada tela) ou outros aspectos relevantes que não foram
explicitados nos fluxos descritos anteriormente. Se não
houverem requerimentos especiais, não remova o item;
escreva simplesmente “Não aplicável.


